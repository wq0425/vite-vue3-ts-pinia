import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { createHtmlPlugin } from "vite-plugin-html";
import data from "./public/data.json";

// https://vite.dev/config/
export default defineConfig({
  base: './',
  server: {
    host: '192.168.1.10',
    port: 8080,
    open: true
  },
  // 设置scss的api类型为modern-compiler
  css: {
    preprocessorOptions: {
      scss: {
        api: 'modern-compiler'
      }
    }
  },
  plugins: [
    vue(),
    createHtmlPlugin({
      inject: {
        data: {
          //将环境变量 VITE_APP_TITLE 赋值给 title 方便 html页面使用 title 获取系统标题
          // title: getViteEnv(mode, "VITE_APP_TITLE"),
          title: data.title
        },
      },
    }),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src')
    }
  },
})
