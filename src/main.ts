import { createApp } from 'vue'
import DataVVue3 from '@kjgl77/datav-vue3';
import pinia from '@/store/index.ts'
import './style.css'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import router from './router'
import VueAutoScroll from '@fcli/vue-auto-scroll';

createApp(App).use(ElementPlus).use(router).use(pinia).use(DataVVue3).use(VueAutoScroll).mount('#app')
