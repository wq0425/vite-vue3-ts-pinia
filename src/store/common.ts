import { defineStore } from 'pinia'
import { ref } from 'vue'
 
const useDemoStore = defineStore('demo', () => {
  const isScreen = ref(true)
 
  const increment = (value: boolean) => {
    isScreen.value = value
  }
 
  return {
    isScreen,
    increment
  }
})
 
export default useDemoStore